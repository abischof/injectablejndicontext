/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.bischinger.jndicontext;

import de.bischinger.jndicontext.impl.JndiContextAnno;
import de.bischinger.jndicontext.impl.JndiSession;
import org.jboss.as.quickstarts.ejb.remote.stateless.RemoteCalculator;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 * @author Alex Bischof
 */
public class RemoteEJBClient {

    @Inject
    @JndiContextAnno
    Instance<Context> contextInstance;

    @Inject
    JndiSession jndiSession;

    /**
     * Looks up a stateless bean and invokes on it
     *
     * @throws javax.naming.NamingException
     */
    void invokeStatelessBean() throws NamingException {
        // Let's lookup the remote stateless calculator
        final RemoteCalculator statelessRemoteCalculator = lookupRemoteStatelessCalculator();
        System.out.println("Obtained a remote stateless calculator for invocation");
        // invoke on the remote calculator
        int a = 204;
        int b = 340;
        System.out.println("Adding " + a + " and " + b + " via the remote stateless calculator deployed on the server");
        int sum = statelessRemoteCalculator.add(a, b);
        System.out.println("Remote calculator returned sum = " + sum);
        if (sum != a + b) {
            throw new RuntimeException("Remote stateless calculator returned an incorrect sum " + sum + " ,expected sum was "
                    + (a + b));
        }
        // try one more invocation, this time for subtraction
        int num1 = 3434;
        int num2 = 2332;
        System.out.println("Subtracting " + num2 + " from " + num1
                + " via the remote stateless calculator deployed on the server");
        int difference = statelessRemoteCalculator.subtract(num1, num2);
        System.out.println("Remote calculator returned difference = " + difference);
        if (difference != num1 - num2) {
            throw new RuntimeException("Remote stateless calculator returned an incorrect difference " + difference
                    + " ,expected difference was " + (num1 - num2));
        }
    }

    /**
     * Looks up and returns the proxy to remote stateless calculator bean
     *
     * @return
     * @throws javax.naming.NamingException
     */
    private RemoteCalculator lookupRemoteStatelessCalculator() throws NamingException {
        Context context = contextInstance.get();
        return (RemoteCalculator) context.lookup("ejb:/jboss-as-ejb-remote-server-side/CalculatorBean!"
                + RemoteCalculator.class.getName());
    }
}
