package de.bischinger.jndicontext.impl;

import javax.inject.Inject;
/**
 * @author Alex Bischof
 */
public class PropertyDecisionLogic {

    @Inject
    JndiSession session;

    public String getHost() {
        return session.getTenantKey() + ".host";
    }

    public String getPort() {
        return session.getTenantKey() + ".port";
    }

    public String getUsername() {
        return session.getTenantKey() + ".username";
    }

    public String getPassword() {
        return session.getTenantKey() + ".password";
    }

    public String getNoanonymous() {
        return session.getTenantKey() + ".noanonymous";
    }
}
