package de.bischinger.jndicontext.impl;

import javax.enterprise.context.ApplicationScoped;

/**
 * @author Alex Bischof
 */
@ApplicationScoped
public class JndiSession {

    private String tenantKey;

    public String getTenantKey() {
        return tenantKey;
    }

    public void setTenantKey(String tenantKey) {
        this.tenantKey = tenantKey;
    }
}
