package de.bischinger.jndicontext.impl;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.naming.Context;
import javax.naming.NamingException;
import java.util.ResourceBundle;

/**
 * @author Alex Bischof
 */
public class JndiContextProducer {

    public static final String TENANT_JBOSS_EJB_CLIENTS = "tenant-jboss-ejb-clients";

    @Inject
    PropertyDecisionLogic propertyDecisionLogic;

    @Produces
    @JndiContextAnno
    public Context create() throws NamingException {

        // Hole Schl�ssel
        String hostKey = propertyDecisionLogic.getHost();
        String portKey = propertyDecisionLogic.getPort();
        String usernameKey = propertyDecisionLogic.getUsername();
        String passwordKey = propertyDecisionLogic.getPassword();
        String noanonymousKey = propertyDecisionLogic.getNoanonymous();

        ResourceBundle bundle = ResourceBundle.getBundle(TENANT_JBOSS_EJB_CLIENTS);

        // Hole values
        String host = bundle.getString(hostKey);
        String port = bundle.getString(portKey);
        String username = bundle.getString(usernameKey);
        String password = bundle.getString(passwordKey);
        Boolean noAnonymous = Boolean.valueOf(bundle
                .getString(noanonymousKey));

        // Setze values
        JBossJndiContextBuilder builder = new JBossJndiContextBuilder();
        builder.setHost(host);
        builder.setPort(port);
        builder.setUser(username);
        builder.setPassword(password);
        builder.setNoanonymous(noAnonymous);

        return builder.build();
    }
}
