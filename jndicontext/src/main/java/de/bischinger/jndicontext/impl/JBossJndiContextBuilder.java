package de.bischinger.jndicontext.impl;

import org.apache.commons.lang.StringUtils;
import org.jboss.ejb.client.ContextSelector;
import org.jboss.ejb.client.EJBClientConfiguration;
import org.jboss.ejb.client.EJBClientContext;
import org.jboss.ejb.client.PropertiesBasedEJBClientConfiguration;
import org.jboss.ejb.client.remoting.ConfigBasedEJBClientContextSelector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;
/**
 * @author Alex Bischof
 */
public class JBossJndiContextBuilder {

    protected String port;
    protected String host;
    protected String user;
    protected String password;
    protected boolean noanonymous;

    public Context build() throws NamingException {
        Properties clientProp = new Properties();
        clientProp
                .put("remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED",
                        "false");
        clientProp.put("remote.connections", "default");

        // Default 4447
        clientProp.put("remote.connection.default.port",
                StringUtils.defaultIfEmpty(port, "8080"));

        // Default 127.0.0.1
        clientProp.put("remote.connection.default.host",
                StringUtils.defaultIfEmpty(host, "127.0.0.1"));

        if (!StringUtils.isEmpty(user)) {
            clientProp.put("remote.connection.default.username", user);
        }
        if (!StringUtils.isEmpty(password)) {
            clientProp.put("remote.connection.default.password", password);
        }

        if (noanonymous) {
            clientProp
                    .put("remote.connection.default.connect.options.org.xnio.Options.SASL_POLICY_NOANONYMOUS",
                            "true");
            clientProp
                    .put("remote.connection.default.connect.options.org.xnio.Options.SASL_DISALLOWED_MECHANISMS",
                            "JBOSS-LOCAL-USER");
        }
        clientProp
                .put("jboss.naming.client.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT",
                        "false");
        System.out.println(Arrays.deepToString(clientProp.entrySet().toArray(
                new Map.Entry[0])));
        EJBClientConfiguration cc = new PropertiesBasedEJBClientConfiguration(
                clientProp);
        ContextSelector<EJBClientContext> selector = new ConfigBasedEJBClientContextSelector(
                cc);
        EJBClientContext.setSelector(selector);

        Properties props = new Properties();
        props.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

        InitialContext initialContext = null;
        try {
            initialContext = new InitialContext(props);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return initialContext;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isNoanonymous() {
        return noanonymous;
    }

    public void setNoanonymous(boolean noanonymous) {
        this.noanonymous = noanonymous;
    }
}