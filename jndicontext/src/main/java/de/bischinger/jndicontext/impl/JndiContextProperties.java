package de.bischinger.jndicontext.impl;
/**
 * @author Alex Bischof
 */
public interface JndiContextProperties {

    public String getHost();

    public String getPort();

    public String getUsername();

    public String getPassword();

    public boolean isNoanonymous();

    public void setHost(String host);

    public void setPort(String port);

    public void setUsername(String username);

    public void setPassword(String password);

    public void setNoanonymous(boolean noanonymous);

}
