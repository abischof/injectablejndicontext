package de.bischinger.jndicontext;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

/**
 * @author Alex Bischof
 */
public class CDIBootStrapper {
    public static void main(String[] args) throws Exception
    {
        WeldContainer weld = new Weld().initialize();
        RemoteEJBClient client = weld.instance().select(RemoteEJBClient.class).get();


        client.jndiSession.setTenantKey("tenant1");
        client.invokeStatelessBean();

        client.jndiSession.setTenantKey("tenant2");
        client.invokeStatelessBean();

    }
}
